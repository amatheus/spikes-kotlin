rootProject.name = "kotlin-spikes"
include("access-delegated-property-delegate")
include("sockets")
include("programming-praxis")