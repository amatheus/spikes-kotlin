plugins {
    kotlin("jvm") version "1.5.31"
}

group = "br.net.andrematheus"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

tasks.withType<Test> {
    useJUnitPlatform()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation(kotlin("test"))
}